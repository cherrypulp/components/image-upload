var __defProp = Object.defineProperty;
var __defProps = Object.defineProperties;
var __getOwnPropDescs = Object.getOwnPropertyDescriptors;
var __getOwnPropSymbols = Object.getOwnPropertySymbols;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __propIsEnum = Object.prototype.propertyIsEnumerable;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __spreadValues = (a, b) => {
  for (var prop in b || (b = {}))
    if (__hasOwnProp.call(b, prop))
      __defNormalProp(a, prop, b[prop]);
  if (__getOwnPropSymbols)
    for (var prop of __getOwnPropSymbols(b)) {
      if (__propIsEnum.call(b, prop))
        __defNormalProp(a, prop, b[prop]);
    }
  return a;
};
var __spreadProps = (a, b) => __defProps(a, __getOwnPropDescs(b));
import require$$0, { useState, useEffect } from "react";
import { Upload, Spin, Image } from "antd";
import ImgCrop from "antd-img-crop";
import { PlusOutlined } from "@ant-design/icons";
var jsxRuntime = { exports: {} };
var reactJsxRuntime_production_min = {};
/**
 * @license React
 * react-jsx-runtime.production.min.js
 *
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
var f = require$$0, k = Symbol.for("react.element"), l = Symbol.for("react.fragment"), m = Object.prototype.hasOwnProperty, n = f.__SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED.ReactCurrentOwner, p = { key: true, ref: true, __self: true, __source: true };
function q(c, a, g) {
  var b, d = {}, e = null, h = null;
  g !== void 0 && (e = "" + g);
  a.key !== void 0 && (e = "" + a.key);
  a.ref !== void 0 && (h = a.ref);
  for (b in a)
    m.call(a, b) && !p.hasOwnProperty(b) && (d[b] = a[b]);
  if (c && c.defaultProps)
    for (b in a = c.defaultProps, a)
      d[b] === void 0 && (d[b] = a[b]);
  return { $$typeof: k, type: c, key: e, ref: h, props: d, _owner: n.current };
}
reactJsxRuntime_production_min.Fragment = l;
reactJsxRuntime_production_min.jsx = q;
reactJsxRuntime_production_min.jsxs = q;
{
  jsxRuntime.exports = reactJsxRuntime_production_min;
}
const jsx = jsxRuntime.exports.jsx;
const jsxs = jsxRuntime.exports.jsxs;
const Fragment = jsxRuntime.exports.Fragment;
const defaultProps = {
  aspect: 1,
  cropOptions: {
    rotate: true
  },
  src: null,
  maxSize: 10,
  name: "image",
  uploadOptions: {
    listType: "picture",
    headers: {
      authorization: "authorization-text"
    },
    onPreview: () => {
    }
  }
};
const ImageUpload = ({
  src,
  onChange,
  cropOptions,
  uploadOptions,
  action,
  aspect,
  uploadButton
}) => {
  const [image, setImage] = useState(src);
  const [loading, setLoading] = useState(false);
  const _onChange = (info) => {
    if (onChange) {
      onChange(info, {
        setImage,
        image,
        loading,
        setLoading
      });
    } else {
      if (info.file.status === "uploading") {
        setLoading(true);
        return;
      }
      if (info.file.status === "done") {
        setImage(info.file.response.url);
        setLoading(false);
      }
    }
  };
  const currentUploadButton = uploadButton || /* @__PURE__ */ jsxs("div", {
    children: [/* @__PURE__ */ jsx(PlusOutlined, {}), /* @__PURE__ */ jsx("div", {
      style: {
        marginTop: 8
      },
      children: " Upload"
    })]
  });
  useEffect(() => {
    setImage(src);
  }, [src]);
  return /* @__PURE__ */ jsx(Fragment, {
    children: /* @__PURE__ */ jsx(ImgCrop, __spreadProps(__spreadValues({
      aspect
    }, cropOptions), {
      children: /* @__PURE__ */ jsx(Upload, __spreadProps(__spreadValues({
        listType: "picture-card",
        name: "image",
        className: "avatar-uploader",
        showUploadList: false,
        action,
        onChange: _onChange
      }, uploadOptions), {
        children: /* @__PURE__ */ jsx(Spin, {
          spinning: loading,
          children: image ? /* @__PURE__ */ jsx(Image, {
            className: "cursor-pointer",
            src: image,
            preview: false
          }) : currentUploadButton
        })
      }))
    }))
  });
};
ImageUpload.defaultProps = defaultProps;
export { ImageUpload, defaultProps };
