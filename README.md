# Image Upload And Crop using Ant Design

Image upload and crop component made easier using AntD.

[![Latest Version on NPM](https://img.shields.io/npm/v/@cherrypulp/image-upload.svg?style=flat-square)](https://npmjs.com/package/@cherrypulp/image-upload)
![npm](https://img.shields.io/npm/dt/@cherrypulp/image-upload)
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.md)

## Demo

A demo is available here : [https://codesandbox.io/s/image-upload-rqfis7?file=/src/App.js](https://codesandbox.io/s/image-upload-rqfis7?file=/src/App.js)

## Getting started

This package works inside an Ant Design Project and assume that you have loaded the Ant Design Style. 

Here is the getting started for the AntD Setup : [https://ant.design/docs/react/getting-started](https://ant.design/docs/react/getting-started). You can also download the minimal CSS to import if you don't fully load Ant Design : [https://github.com/nanxiaobei/antd-img-crop#styles](https://github.com/nanxiaobei/antd-img-crop#styles).

In your project just make :

````bash
yarn add @cherrypulp/image-upload 
# or
npm i @cherrypulp/image-upload --save
````
Then :

````javascript
import {ImageUpload} from "@cherrypulp/image-upload";

const MyComponent = () => {
  return (<>
    <ImageUpload action={"/api/upload"} src={"https://dummyimage.com/600x400/000/fff"} />
    {/* Exemple using a banner with a width 3x larger than height @see https://www.websightdesign.com/helpdesk/article?category=12000001079&folder=5000134798&article=5000638101  */}
    <ImageUpload action={"/api/upload"} aspect={3} src={"https://dummyimage.com/600x400/000/fff"} />
  </>);
}

export default MyComponent;
````

## Fully controlled example

This package give you access to all the properties and features of Ant Img Crop and Ant Uploader.

Here is a working example on how to override default behavior : 

[https://codesandbox.io/s/image-upload-rqfis7?file=/src/App.js](https://codesandbox.io/s/image-upload-rqfis7?file=/src/App.js)

## Properties

| Properties    |          Type           |                    Default                    |                                                                                 Description                                                                                 |
|---------------|:-----------------------:|:---------------------------------------------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
| action *      |         string          |                       -                       |                                                                    Action where the image will be posted                                                                    |
| src*          |         string          |                       -                       |                                                                         Src of the current picture                                                                          |
| defaultSrc    |         string          |                       -                       |                                                                       Default src image file if null                                                                        |
| name          |         string          |                    "image"                    |                                                    default name used to post the picture in the api (default = "image")                                                     |
| aspect        |         number          |                       1                       |                                                                 Ratio of the picture (default = 1 = square)                                                                 |
| cropOptions   |         Object          |               { rotate: true }                |                       Default cropOptions @see [https://github.com/nanxiaobei/antd-img-crop#props](https://github.com/nanxiaobei/antd-img-crop#props)                       |
| uploadOptions |         Object          | { listType : "picture", onPreview: () => {} } | AntD Upload options @see [https://ant.design/components/upload/#components-upload-demo-crop-image](https://ant.design/components/upload/#components-upload-demo-crop-image) |
| uploadButton  | React.ReactNode or null |                       -                       |                                                                 You can override it with a React Component                                                                  |
| maxSize       |         number          |                      10                       |                                                                  Max file size in Mo (default = 10mo max)                                                                   |




## Digging deeper

You can get full documentation or check our complete example :

- [https://ant.design/components/upload/#components-upload-demo-crop-image](https://ant.design/components/upload/#components-upload-demo-crop-image)
- [https://github.com/nanxiaobei/antd-img-crop#props](https://github.com/nanxiaobei/antd-img-crop#props)
- [https://ant.design/components/upload/#API](https://ant.design/components/upload/#API)

# To dos :

[x] Testing e2e

Any helps wanted !

# License

MIT

# Treeware license

This package is also a [Treeware](https://treeware.earth).

If you use it in production, then we kindly ask [**buy the world a tree**](https://plant.treeware.earth/danielsum/@cherrypulp/image-upload) to thank us for our work.

By contributing to the Treeware forest you’ll be creating employment for local families and restoring wildlife habitats.

# Support us

Support us on Open Collective or buy us a Tree :

- [https://opencollective.com/co-labs](https://opencollective.com/co-labs)
