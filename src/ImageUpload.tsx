import React, {useEffect, useState} from 'react';
import {Image, Spin, Upload} from "antd";
import ImgCrop, {ImgCropProps} from "antd-img-crop";
import {PlusOutlined} from "@ant-design/icons";

export const defaultProps = {
    aspect: 1,
    cropOptions: {
        rotate: true
    },
    src: null,
    maxSize: 10,
    name: "image",
    uploadOptions: {
        listType: "picture",
        headers: {
            authorization: 'authorization-text',
        },
        onPreview: () => {
        }
    },
};

export interface ImageUploadProps {
    /**
     * Default source of the image
     */
    defaultSrc?: string
    /**
     * Action to post the pictures
     */
    action?: string;
    /**
     * Current src
     */
    src?: string;
    /**
     * Name used to send the FileRequest
     */
    name?: string,
    /**
     * Name used to send the FileRequest
     */
    onChange?: typeof PropTypes,
    /**
     * Aspect of the picture exemple 1 = square, 2 = width is 2x larger than height
     */
    aspect?: number,
    /**
     * CropOptions
     * @see https://github.com/nanxiaobei/antd-img-crop#props
     */
    cropOptions?: ImgCropProps,
    /**
     * Upload options
     * @see https://ant.design/components/upload/#API
     */
    uploadOptions?: Object,
    uploadButton?: React.ReactNode | null
};

export const ImageUpload: ({
                               src,
                               onChange,
                               cropOptions,
                               uploadOptions,
                               action,
                               aspect,
                               width,
                               height,
                               uploadButton
                           }: ImageUploadProps) => JSX.Element = ({
                                                                      src,
                                                                      onChange,
                                                                      cropOptions,
                                                                      uploadOptions,
                                                                      action,
                                                                      aspect,
                                                                      uploadButton
                                                                  }: ImageUploadProps) => {

    const [image, setImage] = useState(src);
    const [loading, setLoading] = useState(false);

    /**
     * onChange listener from Upload
     *
     * @see https://ant.design/components/upload/#header
     * @param info
     */
    const _onChange = (info: any) => {

        if (onChange) {
            onChange(info, {setImage, image, loading, setLoading});
        } else {
            if (info.file.status === 'uploading') {
                setLoading(true);
                return;
            }
            if (info.file.status === 'done') {
                setImage(info.file.response.url);
                setLoading(false);
            }
        }
    };

    /**
     * The custom uploadButton
     */
    const currentUploadButton = uploadButton || (
        <div>
            <PlusOutlined/>
            <div style={{marginTop: 8}}> Upload</div>
        </div>
    );

    useEffect(() => {
        setImage(src);
    }, [src]);

    return (<>
        <ImgCrop aspect={aspect} {...cropOptions}>
            <Upload
                /*
                // @ts-ignore */
                listType={"picture-card"}
                name={"image"}
                className="avatar-uploader"
                showUploadList={false}
                action={action}
                onChange={_onChange}
                {...uploadOptions}
            >
                <Spin spinning={loading}>{image ? <Image className={"cursor-pointer"} src={image} preview={false}/> : currentUploadButton}</Spin>
            </Upload>
        </ImgCrop>
    </>);
}

ImageUpload.defaultProps = defaultProps;
