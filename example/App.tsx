import 'antd/dist/antd.css';
import './App.css';
import {ImageUpload} from "../src";
import {useState} from "react";
import {Col, Layout, Row, Spin} from "antd";

function App() {

    const [image, setImage] = useState('https://dummyimage.com/600x400/000/fff');
    const [loading, setLoading] = useState(false);

    const handleChange = (info) => {
        if (info.file.status === "uploading") {
            console.log("Uploading state", info);
            setLoading(true);
        } else if (info.file.status === "done") {
            console.log("Response from fakeapi.json", info);

            setTimeout(() => {
                setImage(info.file.response.url);
                setLoading(false);
            }, 3000);
        }
    }

    return (
        <Layout.Content style={{width: 600, margin: "50px auto"}}>
            <h1>Upload example : </h1>
            <Row gutter={10}>
                <Col span={12}>
                    Simple upload component :
                    <ImageUpload action={"/fakeapi.json"} src={"https://dummyimage.com/600x400/000/fff"}
                                 aspect={1}/>
                </Col>
                <Col span={12}>
                    Controlled upload component :
                    <Spin spinning={loading}>
                        <ImageUpload aspect={1} onChange={handleChange} action={"/fakeapi.json"} src={image}
                                     uploadOptions={{
                                         method: "GET", /*For demo purpose, default is POST*/
                                         listType: "picture",
                                         headers: {
                                             authorization: 'authorization-text',
                                         },
                                         onPreview: (e) => {
                                             console.log(e);
                                         }
                                     }}/>
                    </Spin>
                </Col>
            </Row>
        </Layout.Content>
    )
}

export default App
